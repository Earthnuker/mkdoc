# mkdoc.py

Python script to create skeleton for writing papers, assignments, etc

## Example Usage

```
python mkdoc.py -l en -a "Some Guy" -a "Some Other Guy" -s Introduction -s Algorithm -s Implementation -s "Code Listings" "Analysis and Numerics" "Nova-/Newton-Fractal implementation in Rust"
cd nova-newton-fractal-implementation-in-rust
make
```

## Requirements
- [Pandoc](http://pandoc.org/)
- [git](https://git-scm.com/)
- Python 3 instllation with the following modules
    - [jinja2](https://pypi.org/project/Jinja2/)
    - [pyyaml](https://pypi.org/project/PyYAML/)
    - [panflute](https://pypi.org/project/panflute/)
    - [dateutil](https://pypi.org/project/python-dateutil/)
    - [gitpython](https://pypi.org/project/GitPython/)
- [LaTeX](https://en.wikibooks.org/wiki/LaTeX/Installation#Distributions) installation (I use [MikTeX](https://miktex.org/))
- The [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) LaTeX Template
- Optional:
    - [Graphviz](https://graphviz.org/) (for converting DOT files)
    - [Asymptote](http://asymptote.sourceforge.net/) (for converting ASY files)
    - [watchexec](https://crates.io/crates/watchexec) (for `make watch`)

## Installation
```
pip install jinja2 pyyaml panflute dateutil
mkdir -p ~/.pandoc/templates/
wget -O ~/.pandoc/templates/eisvogel.latex https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex
apt install graphviz asymptote
cargo install watchexec
```

## Notes

- Images should ideally be in PDF format and should be placed in the generated `img` folder
- Graphviz DOT files placed inside `src` are automatically converted to PDF images in `img`
- Asymptote ASY files are also automatically converted
- Python scripts with a name starting with `img_` and ending in `.py` are assumed to:
    - take an output filepath as argument on the command line
    - output an image in PDF form to that filename
