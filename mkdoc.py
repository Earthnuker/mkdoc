#!/usr/bin/python3
import os
import sys
import shutil
from jinja2 import Template
import yaml
import datetime
import argparse
import re
from glob import glob
from collections import OrderedDict
import git
from urllib.request import urlretrieve as download
refs_translations = {
    'en': "References",
    'de': "Quellen",
}


def slugify(s):
    s = s.lower()
    for c in [' ', '-', '.', '/']:
        s = s.replace(c, '_')
    s = re.sub('\W', '', s)
    s = s.replace('_', ' ')
    s = re.sub('\s+', ' ', s)
    s = s.strip()
    s = s.replace(' ', '-')
    s = s.replace("ä", "ae")
    s = s.replace("ü", "ue")
    s = s.replace("ö", "oe")
    s = s.replace("ß", "ss")
    return s


def multi_input(prompt):
    print(prompt)
    out = []
    for inp in iter(lambda: input("> ").strip(), ""):
        out.append(inp)
    return out


def ml_input(prompt):
    print(prompt)
    out = ""
    for inp in iter(lambda: input("> ").strip(), ""):
        out += inp + "\n"
    return out.strip()


SCRIPTDIR = os.path.dirname(os.path.abspath(sys.argv[0]))
CWD = os.getcwd()

parser = argparse.ArgumentParser()
parser.add_argument("subject")
parser.add_argument("title")
parser.add_argument("-t", "--type", type=str,
                    choices=["document", "presentation"], default="document")
parser.add_argument("-a", "--author", action="append", default=[])
parser.add_argument("-s", "--section", action="append", default=[])
parser.add_argument("-l", "--language", type=str,
                    choices=refs_translations.keys(), default="de")
parser.add_argument("-c", "--csl", type=str,
                    help="Citation style (from https://github.com/citation-style-language/styles)", default=None)

parser.add_argument("--abstract")

args = parser.parse_args()
filename = slugify(args.title)

head = OrderedDict()
head['title'] = args.title
head['author'] = args.author
if args.subject:
    head['subtitle'] = args.subject
head['date'] = datetime.date.today().strftime("%Y-%m-%d")
if args.abstract:
    head['abstract'] = args.abstract

head['bibliography'] = "src/{}.bib".format(filename)
if args.csl:
    head['csl'] = "src/{}.csl".format(args.csl)

head_s = ""
for k, v in head.items():
    head_s += yaml.dump({k: v}, default_flow_style=False).strip()+"\n"

vars = {
    'head': head_s.strip(),
    'sections': args.section,
    'refs_header': refs_translations[args.language],
    'lang': args.language
}


template = os.path.join(SCRIPTDIR, "templates",
                        "{}.md.jinja".format(args.type))
meta_template = os.path.join(SCRIPTDIR, "templates",
                             "{}.yml.jinja".format(args.type))
with open(template) as tf:
    md_content = Template(tf.read()).render(**vars)

with open(meta_template) as tf:
    yml_content = Template(tf.read()).render(**vars)

os.makedirs(filename)
os.chdir(filename)

for dst in ["filters", "src", "img", "data", "out", "misc"]:
    src = os.path.join(SCRIPTDIR, dst)
    if os.path.isdir(src):
        print("copying", dst)
        shutil.copytree(src, dst)
    elif os.path.isfile(src):
        print("copying", dst)
        shutil.copy(src, dst)
    else:
        print("creating directory", dst)
        os.makedirs(dst)

for src in glob(os.path.join(SCRIPTDIR, "_files_", "*")):
    print("copying", src)
    path, ext = os.path.splitext(src)
    ext = ext[1:]
    if ext.startswith("__") and ext.endswith("__") and ext.strip("_"):
        if ext == "__{}__".format(args.type):
            path = os.path.split(path)[1]
            print(path)
            shutil.copy(src, path)

if args.csl:
    download("https://raw.githubusercontent.com/citation-style-language/styles/master/{}.csl".format(
        args.csl), "src/{}.csl".format(args.csl))[0]


docfn = os.path.join("src", "{}.md".format(filename))
bibfn = os.path.join("src", "{}.bib".format(filename))
metafn = os.path.join("src", "meta.yml")
prefn = os.path.join("src", "preamble.tex")


print("writing", docfn)
with open(docfn, "w") as of:
    of.write(md_content)


print("writing", metafn)
with open(metafn, "w") as of:
    of.write(yml_content)


print("writing", bibfn)
with open(bibfn, "w") as of:
    of.write("")

print("writing", prefn)
with open(prefn, "w") as of:
    of.write("")
